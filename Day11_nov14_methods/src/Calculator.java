
public class Calculator {

	int add(int num1, int num2) {
		int result = num1 + num2;
		return result;
	}

	int subtract(int num1, int num2) {
		return num1 - num2;
	}

	public static void main(String[] args) {
		// creating object of the class Calculator, object reference is c
		Calculator c = new Calculator();

		// calling the method add using object reference(c) and .(dot) operator
		// and storing back the returned value in the sum variable
		// the variable name can be anything of your choice
		int sum = c.add(3, 5);

		// printing the value stored in the sum variable
		System.out.println(sum); // 8

		sum = c.add(3456, 2654);
		System.out.println(sum); // 6110

		System.out.println(c.subtract(45, 65));

		System.out.println(c.subtract(250, 385));

	}

}
