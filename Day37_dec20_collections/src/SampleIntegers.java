import java.util.ArrayList;
import java.util.Collections;

public class SampleIntegers {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(10);
		list.add(20);
		list.add(5);
		list.add(15);
		System.out.println(list);

		Collections.sort(list);
		System.out.println(list);

		Collections.sort(list, Collections.reverseOrder());
		System.out.println(list);

	}
}
