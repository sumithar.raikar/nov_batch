
public class Monkey {

	void eat(Banana banana) {
		System.out.println("Eating banana");
		System.out.println("Banana is of " + banana.color + " color");
	}

	void eat(Chocolate choco) {
		System.out.println("Eating chocolate");
		System.out.println("Chocolate is of " + choco.brand);
	}

	void eat(Cake cake) {
		System.out.println("Eating " + cake.type);
	}

	// think you are the main method
	public static void main(String[] args) {
		// creating an object of Monkey class, m
		Monkey m = new Monkey();

		// creating an object of Banana class, b
		Banana b = new Banana();

		// invoking the monkey class eat method with banana object
		m.eat(b);

		// creating the object of Chocolate class, c
		Chocolate c = new Chocolate();

		// invoking the monkey class eat method with chocolate object
		m.eat(c);

		Cake ck = new Cake();
		m.eat(ck);
	}

}
