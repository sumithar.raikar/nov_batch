
public class User {
	String name;
	String email;
	String password;

	User(String email, String password) {
		this(email, password, null);
	}

	User(String email, String password, String name) {
		this.email = email;
		this.password = password;
		this.name = name;
	}

	// overriding the toString() -> Alt + shift + s + s
	@Override
	public String toString() {
		return "User [name=" + name + ", email=" + email + ", password=" + password + "]";
	}

	public static void main(String[] args) {
		User u = new User("Alpha@gmail.com", "Alpha@123");
		System.out.println(u);
	}

}
