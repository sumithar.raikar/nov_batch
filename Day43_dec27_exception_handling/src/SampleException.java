import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Logger;

public class SampleException {

	public static Logger log = Logger.getLogger(SampleException.class.getName());

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter two integers");
		try {
			int x = s.nextInt();
			int y = s.nextInt();
			int z = x / y;
			log.info("The result of division is " + z);
		} catch (ArithmeticException e) {
			log.severe("Run the prgm again. Please change the denominator " + e);
		} catch (InputMismatchException e) {
			log.severe("Please enter a proper integer value");
		} catch (Exception e) {
			e.printStackTrace();
			log.severe("Some error occured in the program");
		}
		log.info("End of the program");
	}
}
