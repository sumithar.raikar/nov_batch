package animalexample;

public class Animal {
	void eat() {
		System.out.println("Eat with mouth directly");
	}

	public static void main(String[] args) {
		Dog d = new Dog();
		d.eat();

		Monkey m = new Monkey();
		m.eat();
	}
}
