
// converting from lower primitive datatype to higher primitive datatype
public class Widening {
	public static void main(String[] args) {
		byte b = 25;
		short s = b;
		int i = s;
		long l = i;

		System.out.println(b + " " + s + " " + i + " " + l);
	}
}
