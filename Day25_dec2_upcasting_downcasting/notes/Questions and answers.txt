Generalization: 
1. where do we use generalization:

When a method is returning more than one object, we can have only one 
return type to a method, so we write its supertype as return type, if
the objects are not from same category then we write Object class as return type


2. Where do we use upcasting:

When a method is returning either superType or Object class reference as return type
then when we collect at the method call, it is upcasting

When you upcast, we cannot access the sub class specific members, to access the sub
class specific members, we have to downcast


3. Where do we use downcasting:

To access the sub class specific members then we have to downcast
