package vehicleexample;

public class Bike extends Vehicle {
	boolean isKickerAvailable;

	void kick() {
		System.out.println("Kick start");
	}

	@Override
	public String toString() {
		return "Bike [color=" + color + ", brand=" + brand + ", price=" + price + ", isKickerAvailable="
				+ isKickerAvailable + "]";
	}

}
