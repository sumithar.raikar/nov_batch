import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;

public class BankAccount {
	long accountNo;
	String name;
	double balance;

	// Alt + shift + s + O
	BankAccount(long accountNo, String name, double balance) {
		this.accountNo = accountNo;
		this.name = name;
		this.balance = balance;
	}

	@Override
	public int hashCode() {
		return Objects.hash(accountNo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BankAccount other = (BankAccount) obj;
		return accountNo == other.accountNo;
	}

	@Override
	public String toString() {
		return "BankAccount [accountNo=" + accountNo + ", name=" + name + ", balance=" + balance + "]\n";
	}

	public static void main(String[] args) {
		BankAccount b1 = new BankAccount(65473, "Alpha", 4356.0);
		BankAccount b2 = new BankAccount(65473, "Beta", 56098.0);
		BankAccount b3 = new BankAccount(65473, "Charlie", 435665.0);
		BankAccount b4 = new BankAccount(54637, "Delta", 5665.0);

		HashSet<BankAccount> allAccounts = new HashSet<>(Arrays.asList(b1, b2, b3, b4));
		System.out.println(allAccounts);
	}

}
