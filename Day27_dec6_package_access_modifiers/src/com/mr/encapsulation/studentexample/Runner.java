package com.mr.encapsulation.studentexample;

public class Runner {
	public static void main(String[] args) {
		Student s = new Student();
		s.setAge(34);
		s.setName("Alpha");
		s.setPhoneNo(9974454647L);

		System.out.println(s.getAge()); // 34
	}
}
